var _ = require('lodash'),
    $ = require('jquery'),
    fs = require('fs'),
    remote = require('remote'),
    browser = require('airplay-js').createBrowser(),
    icy = require('icy'),
    Writable = require('stream').Writable,
    availableStations = [],
    selectedStation = {},
    devices = [],
    selectedDeviceID = 0,
    ws = null;

// http://www.nederland.fm/common/radio/zenders/android/www/nederland.js
// http://www.nederland.fm/common/radio/zenders/nederland.js

browser.on('deviceOn', function(device) {
    devices.push(device);
    console.log('Device online: ', device);
    renderDevices();
});

browser.on('deviceOff', function(device) {
    console.log('Device offline: ', device);
    renderDevices();
});

browser.start();

$(document).ready(function() {
    $('img').error(function(){
        $(this).hide();
    });

    $('.header').on('click', '.maximize', function () {
        var window = remote.getCurrentWindow();
        window.isMaximized() ? window.unmaximize() : window.maximize();
    });

    $('.header').on('click', '.close', function () {
        var window = remote.getCurrentWindow();
        window.close();
    });

    $('.header').on('keyup search', '.search', function () {
        var searchString = $(this).val(),
            searchResults = _.filter(availableStations, function(item) {
                return item.o.toLowerCase().indexOf(searchString.toLowerCase()) > -1;
            });

        renderChannels(searchResults);
    });

    $('.devices').on('click', 'li', function () {
        selectedDeviceID = $(this).data('deviceid');
        playStream();
    });

    $('.volume').on('input', function () {
        var volume = $(this).val() / 10;

        _.findWhere(devices, { id: selectedDeviceID }).volume(volume, function() {
            console.info('Volume change');
        });
    });

    $.get('http://www.nederland.fm/common/radio/zenders/nederland.js', function (){
        availableStations = _.filter(zenders.items, function (station) {
            return !_.isEmpty(station.m) || !_.isEmpty(station.jp) || !_.isEmpty(station.ios);
        });
        renderChannels(availableStations);
    });

    $('.stationList').on('click', 'li', function () {
        var statonID = $(this).data('stationid');

        selectedStation = _.findWhere(availableStations, { z: statonID });
        playStream();
    });

});

var renderDevices = function () {
    $('.devices ul').empty();

    _.forEach(devices, function (device) {
        if (device.id === selectedDeviceID) {
            $('.devices ul').append('<li class="selected" data-deviceID="' + device.id + '"><span class="fa fa-check"></span> ' + device.name + '</li>');
        }
        else{
            $('.devices ul').append('<li data-deviceID="' + device.id + '">' + device.name + '</li>');
        }
    })
};

var renderChannels = function (stations) {
    $('.stationList').empty();

    _.forEach(stations, function (station) {
        $('.stationList').append('<li data-stationID="' + station.z + '"><img src="http://www.nederland.fm/i/l/' + station.z + '.gif"/></li>');
    })
};

var createStream = function () {
    if (ws !== null)
        ws.end();

    ws = Writable();
    ws._write = function (chunk, enc, next) {
        next();
    };
};

var playStream = function () {
    var streamUrl;

    $('.player img').attr('src', 'http://www.nederland.fm/i/l/' + selectedStation.z + '.gif');
    $('.player .stationName').text(selectedStation.o);

    if (!_.isEmpty(selectedStation.jp)) {
        streamUrl = selectedStation.jp;
    }
    else if (!_.isEmpty(selectedStation.m)) {
        streamUrl = selectedStation.m;
    }
    else if (!_.isEmpty(selectedStation.ios)) {
        streamUrl = selectedStation.ios;
    }

    _.findWhere(devices, { id: selectedDeviceID }).play(streamUrl, 0, function() {
        console.info('Streaming: ' + streamUrl);

        icy.get(streamUrl, function (res) {
            createStream();

            // log the HTTP response headers
            //console.log(res.headers);

            res.on('metadata', function (metadata) {
                var streamInfo = icy.parse(metadata);
                $('.player .streamTitle').text(' - ' + streamInfo.StreamTitle);
            });
            res.pipe(ws);
        });
    });

    console.log('Stream: ' + streamUrl + ', to: ' + selectedDeviceID);

};